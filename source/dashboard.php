<?php
  if(isset($_GET['token'])) {
      $token = $_GET['token'];
  } else {
    header("Location: ../index.html");
    exit;
  }
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <link rel="icon" href="../img/logo/logo-pttep.png" type="image/png" sizes="17x17">
  <title>PTTEP</title>
  <!-- Font Awesome -->
  <link href="../font/fontawesome/css/all.min.css" rel="stylesheet">
  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="../css/mdb.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="../css/style.min.css" rel="stylesheet">
  <!-- Your custom datetimepicker (optional) -->
  <link href="../css/jquery.datetimepicker.min.css" rel="stylesheet">
  <!-- Your custom dataTable (optional) -->
  <link href="../css/datatables.css" rel="stylesheet">
  <style>

  .map-container{
    overflow:hidden;
    padding-bottom:56.25%;
    position:relative;
    height:0;
  }
  .map-container iframe{
    left:0;
    top:0;
    height:100%;
    width:100%;
    position:absolute;
  }

  .tr_color {color: #F5F5F5;}
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container-fluid">

        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

          <!-- Left -->
          <ul class="nav nav-pills nav-pills-primary mr-auto" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#link1" role="tablist" aria-expanded="true">
                <i class="fas fa-chart-bar mr-1"></i> <B>DASHBOARD</B>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " data-toggle="tab" href="#link2" role="tablist" aria-expanded="false">
                <i class="fas fa-info-circle mr-1"></i> <B>RAW DATA</B>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link " data-toggle="tab" href="#link3" role="tablist" aria-expanded="false">
                <i class="fas fa-tram"></i> <B>DEVICE MANAGER</B>
              </a>
            </li>
          </ul>

          <!-- Right -->
          <ul class="navbar-nav nav-flex-icons">
            <li class="nav-item">
              <B>WIRELESS SEISMIC DAQ</B>
            </li>

          </ul>

        </div>

      </div>
    </nav>
    <!-- Navbar -->

    <!-- Sidebar -->
    <div class="sidebar-fixed position-fixed">

      <!-- <a class="logo-wrapper waves-effect"> -->
      <div class="mb-4">
        <center><img src="../img/logo/logo-pttep.png"  alt="" style="width: 50%; height: 50%"></center>
      </div>
      <!-- </a> -->

      <B>Menu Config</B>
      <div class="mb-2"></div>

      <table class="table table-hover">
        <tbody>
          <tr>
            <th>Device ID</th>
            <td id="side_id">WS000001</td>
          </tr>
          <tr>
            <th>TimeStamp</th>
            <td id="real_date"></td>
          </tr>
          <tr>
            <th>Status</th>
            <td id="side_status">ONLINE</td>
          </tr>
          <!-- <tr>
          <th>Start<i class="far fa-calendar ml-1"></i></th>
          <td><input type="text" size="11" id="start_date"></td>
        </tr>
        <tr>
        <th>Stop<i class="fas fa-calendar ml-1"></i></th>
        <td><input type="text" size="11" id="end_date"></td>
      </tr> -->
      <tr>
        <!-- <th>Start/Stop</th>
        <td>
          <label class="bs-switch">
            <input type="checkbox">
            <span class="slider round"></span>
          </label>
        </td> -->
        <td>
          <button type="button" class="btn btn-success btn-sm" id="record_data">Record</button>
        </td>
        <td>
          <button type="button" class="btn btn-danger btn-sm" id="stop_data">Stop</button>
        </td>
      </tr>
    </tbody>
  </table>

</div>
<!-- Sidebar -->

</header>
<!--Main Navigation-->

<!--Main layout-->
<main class="pt-5 mx-lg-5">
  <div class="container-fluid mt-5">
    <div class="tab-content tab-space">

      <!-- Page 1 -->
      <div class="tab-pane active" id="link1" aria-expanded="true">

        <!-- Graph CH1 -->
        <div class="col-md-12 mb-4 fadeIn">

          <div class="card">

            <div href="#CH1" class="card-header waves-effect" data-toggle="collapse" id="GCH1"><i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph CH1</B></div>

            <div class="card-body collapse" id="CH1">
              <div id="ch1" style="height: 400px; min-width: 310px"></div>
            </div>

          </div>

        </div>
        <!-- Graph CH2 -->
        <div class="col-md-12 mb-4 fadeIn">

          <div class="card">

            <div href="#CH2" class="card-header waves-effect" data-toggle="collapse" id="GCH2"><i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph CH2</B></div>

            <div class="card-body collapse" id="CH2">
              <div id="ch2" style="height: 400px; min-width: 310px"></div>
            </div>

          </div>

        </div>
        <!-- Graph CH3 -->
        <div class="col-md-12 mb-4 fadeIn">

          <div class="card">

            <div href="#CH3" class="card-header waves-effect" data-toggle="collapse" id="GCH3"><i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph CH3</B></div>

            <div class="card-body collapse" id="CH3">
              <div id="ch3" style="height: 400px; min-width: 310px"></div>
            </div>

          </div>

        </div>
        <!-- Google Map -->
        <div class="row">
          <div class="col-md-4 mb-4">

            <div class="card">

              <div href="#map1" class="card-header waves-effect" data-toggle="collapse" id="map_box"><i class="far fa-map fa-2x mr-2"></i> <B>Google map</B></div>

              <div class="card-body collapse" id="map1">
                <div id="map" class="z-depth-1-half" style="height: 350px"></div>
              </div>

            </div>

          </div>
          <div class="col-md-4 mb-4">

            <div class="card">

              <div href="#batt" class="card-header waves-effect" data-toggle="collapse" id="batt_box"><i class="fas fa-car-battery fa-2x mr-2"></i> <B>BATTERY</B></div>

              <div class="card-body collapse text-center" id="batt">
                <h4><B> BATTERY STATUS</B></h4>
                <div id="bat_img">
                  <i class="fas fa-battery-empty" style="font-size: 100px;"></i>
                </div>
                <h2 ><B id="battery">0%</B></h2>
              </div>

            </div>

          </div>

          <div class="col-md-4 mb-4">

            <div class="card">

              <div href="#temp" class="card-header waves-effect" data-toggle="collapse" id="temp_box"><i class="fas fa-temperature-high fa-2x mr-2"></i> <B>TEMPERATURE ( Celcius )</B></div>

              <div class="card-body collapse text-center" id="temp">
                <h4><B> TEMP STATUS</B></h4>
                <!-- <div id="temp_img" class="mb-6">
                <i class="fas fa-thermometer-quarter" style="font-size: 100px;"></i>
              </div> -->
              <p style="font-size:5vw;"><B id="temper" >0</B></p>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Page 2 -->
    <div class="tab-pane " id="link2" aria-expanded="true">
      <!-- Graph RAW -->
      <div class="col-md-12 mb-4 fadeIn">
        <div class="row">
          <div class="col-md-2"></div>
          <div class="col-md-8  fadeIn">
            <div class="card ">
              <div class="container">
                <div class="col-12">
                  <div class="row">
                    <div class="col-5">
                      <p class="mt-2">START DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Start Date" aria-label="Start_Date" id="start_date_row" value="">
                      </div>
                    </div>
                    <div class="col-5">
                      <p class="mt-2">END DATE</p>
                      <div class="input-group mt-3 mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="fas fa-calendar-week"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="End Date" aria-label="End_Date" id="end_date_row" value="">
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="mt-4 mb-3">
                        <button type="button" class="btn blue-gradient btn-rounded" id="search">search</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12 mb-4 fadeIn">
        <div class="card">
          <div href="#row_coll" class="card-header waves-effect" data-toggle="collapse" id="GROW"><i class="fas fa-chart-line fa-2x mr-2"></i><B>Graph</B></div>

          <div class="card-body collapse" id="row_coll">
            <div id="ch3" style="height: 400px; min-width: 310px"></div>
          </div>
        </div>

      </div>

      <h2>RAW Table</h2>
      <hr>
      <div class="head_table table-responsive">
        <table class="table table-hover table-bordered" id="row_table">
          <thead>
            <tr class="bg-primary tr_color">
              <th>ID Device</th>
              <th>Channel</th>
              <th>UTC</th>
              <th>TIME</th>
              <th>Payload</th>
            </tr>
          </thead>
          <tbody id="table_body">

          </tbody>
        </table>
      </div>

      <center><div class="loader" id="row_load2"></div></center>

    </div>

    <!-- page 3 -->

    <div class="tab-pane " id="link3" aria-expanded="true">
      <form action="api/device_api.php" method="get">
        <input type="checkbox" name="index" value="" id="index" checked style="display:none;">
        <div class="col-md-12 mb-4 fadeIn">
          <div class="container-fluid ">
            <br>
            <h1>DEVICE</h1>
            <hr>
            <div class="table-responsive">
              <table class="table table-hover table-bordered" id="device_table">
                <thead align="center">
                  <tr class="bg-primary tr_color">
                    <th scope="col" width="5%">Choose</th>
                    <th scope="col" width="5%">NO.</th>
                    <th scope="col" width="20%">Device ID</th>
                    <th scope="col" width="20%">Latitude</th>
                    <th scope="col" width="20%">Longitude</th>
                    <!-- <th scope="col" width="20%">Port</th> -->
                    <th scope="col" width="10%">Status</th>
                  </tr>
                </thead>
                <tbody id="device_table_body" align="center">

                </tbody>
              </table>
            </div>
          </div>
        </div>
        <input type="submit" value="Submit" id="sent_data" hidden="hidden">
        <!-- hidden="hidden" -->
      </form>
    </div>

  </div>
</main>
<!--Main layout-->

<!--Footer-->
<footer class="page-footer text-center font-small primary-color-dark darken-2 mt-4 wow fadeIn">

  <!--Copyright-->
  <div class="footer-copyright py-2">
    © 2018 Copyright:
    <a href="http://www.pttep.com/en/index.aspx" target="_blank"> pttep.com </a>
  </div>
  <!--/.Copyright-->

</footer>
<!--/.Footer-->

<!-- SCRIPTS -->

<!-- JQuery -->
<script type="text/javascript" src="../js/jquery-3.3.1.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="../js/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="../js/bootstrap.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="../js/mdb.js"></script>
<!-- fontawesome -->
<script type="text/javascript" src="../font/fontawesome/js/all.min.js"></script>
<!-- datetimepicker -->
<script type="text/javascript" src="../js/jquery.datetimepicker.full.min.js"></script>
<!-- dataTables -->
<script type="text/javascript" src="../js/datatables.js"></script>
<!-- Moment -->
<script type="text/javascript" src="../js/moment.js"></script>
<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_5EUIpqkV5r2TAhksHU_Mu_mbZ4lxtpI&callback=initMap" async defer></script>
<!-- Highstock -->
<script type="text/javascript" src="../js/addons/Highstock.js"></script>
<script type="text/javascript" src="../js/modules/exporting.js"></script>
<script type="text/javascript" src="../js/modules/export-data.js"></script>

<!-- JS Function -->
<script type="text/javascript" src="../js/function/page1.js"></script>
<script type="text/javascript" src="../js/function/page2.js"></script>
<script type="text/javascript" src="../js/function/page3.js"></script>
<script type="text/javascript" src="../js/function/option.js"></script>

<!-- Initializations -->
<script type="text/javascript">

var graph_arr=[];
var map,table_s;
var device_status,device_id="WS000001";

$( document ).ready(function() {
  console.log("ready!");
  // starting-----------------------------------------

  $('#start_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  $('#end_date_row').val(moment().format('YYYY-MM-DD HH:mm:ss'));
  // $('#start_date_row').val(moment().format('2019-02-05 12:11:53')); //test
  // $('#end_date_row').val(moment().format('2019-02-05 12:11:54')); //test

  try {
    initMap();
    input_date();
    btn_start();
    new WOW().init();
  } catch (e) {
    location.reload();
  }

  // page 1-------------------------------------------
  // setInterval(function(){   get_realtime(); }, 1000);
  get_realtime();
  batt(27);

  // page 2-------------------------------------------
  get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));

  // page 3-------------------------------------------

  get_device();

  // option-------------------------------------------
  real_date();

  // test---------------------------------------------
  console.log(	new Date().getTime() );

});

// starting
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 13.8557611, lng: 100.4783308},
    zoom: 8
  });
}

function input_date() {

  // select date RAW
  $('#start_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });
  $('#end_date_row').datetimepicker({
    timepicker:true,
    format:'Y-m-d H:i:s'
  });

  // select date Sise nav
  // $('#start_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });
  // $('#end_date').datetimepicker({
  //   timepicker:false,
  //   format:'d-m-Y'
  // });

}

function btn_start() {
  // page 1
  $('#GCH1').click();
  $('#GCH2').click();
  $('#GCH3').click();
  $('#map_box').click();
  $('#batt_box').click();
  $('#temp_box').click();
  $('#temper').text(30);

  // page 2
  $('#GROW').click();

  $('#search').click(function() {
    get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));
  });

  $('#record_data').click(function() {
    $('#sent_data').click();
  });

}



</script>
</body>
</html>
