// page 2
function graph_row(graph_x,graph_ch1_y,graph_ch2_y,graph_ch3_y) {
  var start = +new Date();
  Highcharts.chart('row_coll', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    title: {
      text: 'Channel'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      }
    },
    plotOptions: {
      series: {
        allowPointSelect: true
      }
    },
    series: [
              {
                name: "CH1",
                data: graph_ch1_y
              },
              {
                name: "CH2",
                data: graph_ch2_y
              },
              {
                name: "CH3",
                data: graph_ch3_y
              }
            ]
  });
}

function get_row(time_s,time_e) {

  // into table
  try {
    table_s.destroy();
    $("#head_table").empty();
    $('#head_table').append(
      '<table class="table table-hover table-bordered" id="row_table">'+
      '<thead>'+
      '<tr class="bg-primary">'+
      '<th>ID Device</th>'+
      '<th>Channel</th>'+
      '<th>UTC</th>'+
      '<th>TIME</th>'+
      '<th>Payload</th>'+
      '</tr>'+
      '</thead>'+
      '<tbody id="table_body">'+

      '</tbody>'+
      '</table>'+
      '<center><div class="loader" id="row_load2"></div></center>'
    );
    $("#table_body").empty();
  } catch (e) {
    console.log(e);
  }


  console.log('..get_raw..');
  // console.log(time_s);
  // console.log(time_e);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://35.240.232.232:1880/ssmic?id="+device_id+"&st="+time_s+"&ed="+time_e,
    // "url": "http://35.240.232.232:1880/ssmic?ch=1&id=WS20180001&st=1545619894196&ed=1545619897796", //test
    // "url": "http://127.0.0.1/Deaware/PTT_demo/mockup/daq.json", //mockup
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {

// --------------------------------------------------------------------------------------
    try {
      console.log(response[0].payload);

      var graph_ch1_x=[],graph_ch1_y=[];
      var graph_ch2_x=[],graph_ch2_y=[];
      var graph_ch3_x=[],graph_ch3_y=[];

      var utc_plus=0;
      var utc_plus_val=0;

      for(var index=0;index<response.length;index++){
        var val = response[index].payload;

        utc_plus = response[index].UTC;
        utc_plus_val = Math.floor((1/response[index].sampling)*(1000));

        for(var index2=0;index2<val.length;index2++){

          for(var index3=0;index3<val[index2].data.length;index3++){

            $("#table_body").append(
              '<tr>'+
              '<td>'+response[index].id+'</td>'+
              '<td>'+val[index2].ch+'</td>'+
              '<td>'+utc_plus+'</td>'+
              '<td>'+moment(utc_plus).format("YYYY-MM-DDTH:mm:ss:SSS Z")+'</td>'+
              '<td>'+val[index2].data[index3]+'</td>'+
              '</tr>'
            );

            switch(val[index2].ch) {
              case 1:
              graph_ch1_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch1_y.push(val[index2].data[index3]);
              break;
              case 2:
              graph_ch2_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch2_y.push(val[index2].data[index3]);
              break;
              case 3:
              graph_ch3_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch3_y.push(val[index2].data[index3]);
              break;
              default:
              console.log("Empty Value!!");
            }
            utc_plus+=utc_plus_val;
          }
        }
        utc_plus=0;
      }
    } catch (e) {
      console.log(e);
    } finally {
      graph_row(graph_ch1_x,graph_ch1_y,graph_ch2_y,graph_ch3_y)
    }
// --------------------------------------------------------------------------------------
    table_s = $("#row_table").DataTable({
      destroy: true,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'csv',
          text: 'CSV',
          exportOptions: {
            modifier: {
              search: 'none'
            }
          }
        }
      ]
    });
    $("#row_load2").hide();
  });
}
