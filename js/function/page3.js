// page 3

$( document ).ready(function() {

  // var time_e = moment().format('X');
  var time_e_device = "1549283710295" //test
  var time_s_device = parseInt(time_e_device)-1000;
  time_s_device = time_s_device.toString();

  $("#device_table_body").on('click','tr',function(e){

    var table = $("#device_table_body");
    var $tds = $(this).find('td');
    var id = $tds.eq(2).text();
    var status = $tds.eq(6).text();
    console.log(id+"-"+status);

    var r = confirm("You are sure to choose "+id);
    if (r == true) {
      $('#side_id').text(id);
      $('#side_status').text(status);
      device_id=id;
      device_status=status;
      get_row(time_set($('#start_date_row').val()),time_set($('#end_date_row').val()));
    }

  });
});

function get_device() {
  console.log('..get_device..');
  // console.log(time_s);
  // console.log(time_e);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://35.240.232.232:1880/device",
    // "url": "http://35.240.232.232:1880/ssmic?ch=1&id=WS20180001&st=1545619894196&ed=1545619897796", //test
    // "url": "http://127.0.0.1/Deaware/PTT_demo/GO/daq.json", //mockup
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {
    console.log(response);
    // mockup
    // var lat="0.00",long="0.00";
    // var port="1880";

    var val=response.reverse();
    $("#index").val(val.length);
    for(var i=0;i<val.length;i++){

      $("#device_table_body").append(
        '<tr>'+
          '<td>'+
            '<div class="custom-control custom-checkbox">'+
              '<input type="checkbox" class="custom-control-input" id="checked'+i+'" name="checked'+i+'" value="'+val[i].id+'">'+
              '<label class="custom-control-label" for="checked'+i+'"></label>'+
            '</div>'+
          '</td>'+
          '<td>'+(i+1)+'</td>'+
          '<td>'+val[i].id+'</td>'+
          '<td>'+val[i].lat+'</td>'+
          '<td>'+val[i].long+'</td>'+
          // '<td>'+val[i].port+'</td>'+
          '<td>'+
          '<button type="button" class="btn btn-outline-success btn-sm m-0 waves-effect"><B>ONLINE</B></button>'+
          '</td>'+
        '</tr>'
      );
    }

    var table_d = $("#device_table").DataTable({
      destroy: true,
      dom: 'Bfrtip',
      buttons: [
        {
          extend: 'csv',
          text: 'CSV',
          exportOptions: {
            modifier: {
              search: 'none'
            }
          }
        }
      ]
    });
  });
}
