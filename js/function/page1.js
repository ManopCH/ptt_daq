// page 1
function graph1(graph_x,graph_y) {
  var start = +new Date();
  Highcharts.chart('ch1', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    title: {
      text: 'CH1'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      }
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function graph2(graph_x,graph_y) {
  var start = +new Date();
  Highcharts.chart('ch2', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    title: {
      text: 'CH2'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      }
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function graph3(graph_x,graph_y) {
  var start = +new Date();
  Highcharts.chart('ch3', {
    chart: {
      type: 'spline',
      zoomType: 'x',
      events: {
        load: function () {
          if (!window.TestController) {
            this.setTitle(null, {
              text: 'Built chart in ' + (new Date() - start) + 'ms'
            });
          }
        }
      }
    },
    title: {
      text: 'CH3'
    },
    subtitle: {
      text: 'Built chart in ...'
    },
    credits: {
      enabled: false
    },
    xAxis: {
      categories: graph_x
    },
    yAxis: {
      title: {
        text: 'VOLTAGE (V)'
      }
    },
    plotOptions: {
      series: {
        animation: false,
        allowPointSelect: true
      }
    },
    series: [{
      data: graph_y
    }]
  });
}

function get_realtime() {
  console.log('..get_realtime..');

  var time_e = moment().format('x');
  // var time_e = "1549343513317" //test
  var time_s = parseInt(time_e)-1000;
  time_s = time_s.toString();

  // console.log(time_s);
  // console.log(time_e);

  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://35.240.232.232:1880/ssmic?id="+device_id+"&st="+time_s+"&ed="+time_e,
    // "url": "http://35.240.232.232:1880/ssmic?id=WS000001&st=1549278201554&ed=1549278203554", //test
    // "url": "http://127.0.0.1/Deaware/PTT_demo/mockup/daq.json", // mockup
    "method": "GET"
  }

  $.ajax(settings).done(function (response) {

    try {
      console.log(response[0].payload);

      var graph_ch1_x=[],graph_ch1_y=[];
      var graph_ch2_x=[],graph_ch2_y=[];
      var graph_ch3_x=[],graph_ch3_y=[];

      var utc_plus=0;
      var utc_plus_val=0;

      for(var index=0;index<response.length;index++){
        // console.log("index : "+index);
        var val = response[index].payload;

        utc_plus = response[index].UTC;
        utc_plus_val = Math.floor((1/response[index].sampling)*(1000));
        // console.log(utc_plus_val);

        for(var index2=0;index2<val.length;index2++){
          // console.log(val[index2].data);

          for(var index3=0;index3<val[index2].data.length;index3++){
            // console.log(val[index2].data[index3]);

            switch(val[index2].ch) {
              case 1:
              graph_ch1_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch1_y.push(val[index2].data[index3]);
              break;
              case 2:
              graph_ch2_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch2_y.push(val[index2].data[index3]);
              break;
              case 3:
              graph_ch3_x.push(moment(utc_plus).format("H:mm:ss:SSS"));
              graph_ch3_y.push(val[index2].data[index3]);
              break;
              default:
              console.log("Empty Value!!");
            }
            utc_plus+=utc_plus_val;
          }
        }
        utc_plus=0;
      }
    } catch (e) {
      console.log(e);
    } finally {
      graph1(graph_ch1_x,graph_ch1_y);
      graph2(graph_ch2_x,graph_ch2_y);
      graph3(graph_ch3_x,graph_ch3_y);
    }

  });
}

function batt(val) {

  var font_size = "110px";
  $('#bat_img').empty();

  if(val>=100){
    $('#bat_img').append('<i class="fas fa-battery-full" style="font-size: '+font_size+'; color: #70db70;"></i>');
    $('#battery').text(val+"%");
  }else if(val>=75 && val<100){
    $('#bat_img').append('<i class="fas fa-battery-three-quarters" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=50 && val<=74){
    $('#bat_img').append('<i class="fas fa-battery-half" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=25 && val<=49){
    $('#bat_img').append('<i class="fas fa-battery-quarter" style="font-size: '+font_size+';"></i>');
    $('#battery').text(val+"%");
  }else if(val>=0 && val<=24){
    $('#bat_img').append('<i class="fas fa-battery-empty" style="font-size: '+font_size+'; color: #ff6666;"></i>');
    $('#battery').text(val+"%");
  }else{}
}
